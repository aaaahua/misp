package edu.mis.modules.customer;

import com.jfinal.core.Controller;

import edu.mis.model.Customer;
import edu.mis.modules.customer.CustomerService;

public class CustomerController extends Controller {
	
	static CustomerService service = new CustomerService();
	
	/**
	 * 日志列表视图页面
	 */
	public void index() {   //如果为index，则访问地址为"路由名/" ,此处为"customer/"
		render("customer.html");
	}
	
	/**
	 * 日志列表后台json
	 */
	
	public void list(){   //访问地址为 customer/list
		setAttr("data", service.findAllCustomer());
		renderJson();   //可直接在浏览器输入"localhost:8080/customer/list"查看返回的Json值，可以作为调试的方法
	}


	/**
	 * 添加日志
	 */
	public void save(){
		Customer customer  =  getModel(Customer.class); //注意：使用getModel方法获取表单值非常方便，不需要对每个字段进行设置，直接得到model。注意使用此方法前端表单name必须名称为“对象名.属性名”，如Customer.title   其中 customer对应Customer类，title对应Customer类的title属性
		if(service.saveCustomer(customer)){
			setAttr("result", true); //前端ajax回调值，进行交互判断  if(data.result){.......} 注意两个result对应
			setAttr("msg", "添加公告成功!"); //前端ajax回调值，交互消息框layer.msg(data.msg)
		}else{
			setAttr("result",false);
		}
		renderJson();  //返回json，用于向前台页面返回结果
	}
	
	public void edit() {
		setAttr("customer", service.findCustomerById(getParaToInt("id")));  //getPara获取参数为string类型，getParaToInt将获得的String转为int
		renderJson();
	}
	
	/**
	 * 修改日志
	 */
	public void update(){
		Customer customer  =  getModel(Customer.class);
		if(service.updateCustomer(customer)){
			setAttr("result", true);
			setAttr("msg", "修改公告成功!");
		}else{
			setAttr("result", false);
		}
		renderJson();
	}
	
	/**
	 * 删除日志
	 * 测试访问地址：localhost:8080/Customer/delete?id=1
	 */
	public void delete() {
		if(service.deleteCustomerById(getParaToInt("id"))){
			setAttr("result", true);
			setAttr("msg", "删除公告成功!");
		}else{
			setAttr("result", false);
		}
		renderJson();
	}
	
}


