package edu.mis.modules.customer;

import java.util.List;

import edu.mis.model.Customer;

public class CustomerService {
	
		
		/**
		 * 添加日志
		 * @param customer
		 * @return
		 */
		public boolean saveCustomer(Customer customer){  //service命名建议完整，见名知意。如此处的saveCustomer，参数尽量用对象
			return customer.save();
		}
		
		/**
		 * 修改日志
		 * @param customer
		 * @return
		 */
		public boolean updateCustomer(Customer customer){
			return customer.update();
		}
		
		/**
		 * 删除日志
		 * @param id
		 * @return
		 */
		public boolean deleteCustomerById(int id) {
			return Customer.dao.deleteById(id);
		}
		
		/**
		 * 根据ID得到日志
		 * @param id
		 * @return
		 */
		public Customer findCustomerById(int id) {
			return Customer.dao.findById(id);
		}
		
		/**
		 * 查询所有日志
		 * @return
		 */
		public List<Customer> findAllCustomer(){   //本项目使用dataTables支持前端分页。如果要支持后端分页，可调用Customer.dao.paginate(pageNumber, pageSize, sqlPara)
			return Customer.dao.findAll();
		}
	}
