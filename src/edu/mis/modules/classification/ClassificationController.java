package edu.mis.modules.classification;

import com.jfinal.core.Controller;

import edu.mis.model.Classification;
import edu.mis.model.Classification;
import edu.mis.model.Classification;

/**
 * 权限控制器
 *
 */
public class ClassificationController extends Controller {
	
	static ClassificationService service = new ClassificationService();
	
	public void index() {
		render("classification.html");
	}
	
	public void list(){
		setAttr("data", service.findAllClassification());
		renderJson();  
	}
	
	/**
	 * 修改备注视图（默认对应edit.html，否则需显性指定，如render("editRemark.html")，）
	 */
	public void edit() {
		setAttr("classification", service.findClassificationById(getParaToInt("id")));  //此处classification应与表单中${(classification.name)!}等中的classification保持一致。
	}
	
	public void updateRemark(){
		int id=this.getParaToInt("id");   //使用getPara获取表单值，与getModel方法获得表单值不同，name不能使用对象名.属性名，如classification.remark，而直接使用属性名，如remark
		String newRemark = this.getPara("remark");
		if(service.updateRemark(id, newRemark)){
			setAttr("result", true);
			setAttr("msg", "修改类别成功!");
		}else{
			setAttr("result", false);
		}
		renderJson();
	}
	/**
	 * 注册用户视图
	 */
	public void add() {
		    //render("add.html");  //（默认对应add.html，否则需显性指定，如render("addClassification.html")）
	}

	public void register(){
		Classification classification  =  getModel(Classification.class);   //使用getModel方法获取表单值，表单中name应使用对象名.属性名，如classification.id, classification.price,详见jfinal手册
		if(service.register(classification)){
			setAttr("result", true); 
			setAttr("msg", "新建类别成功!"); 
		}else{
			setAttr("result",false);
			setAttr("msg", "新建失败，类别名重复!"); 
		}
		renderJson(); 
	}

}


