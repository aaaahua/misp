package edu.mis.modules.classification;

import java.util.List;

import edu.mis.model.Classification;
import edu.mis.model.Classification;
import edu.mis.model.Classification;

/**
 * 权限服务类
 *
 */
public class ClassificationService {
	
	/**
	 * 查询所有权限
	 * @return
	 */
	public List<Classification> findAllClassification(){
		return Classification.dao.findAll();
	}
	/**
	 * 注册
	 * @param classification
	 * @return
	 */
	public boolean register(Classification classification){
		if(classification.findClassificationByName(classification.getClassificationName())==null){ //此处为业务逻辑，写在service里，不要写在model里。判断是否用户名重复，
			return classification.save();
		}else{
			return false;
		}
		
	}
	
	/**
	 * 修改备注
	 * @param classification
	 * @return
	 */
	public boolean updateRemark(int id,String newRemark){
		Classification classification = findClassificationById(id);
		classification.setRemark(newRemark);
		return classification.update();
	}
	
	/**
	 * 根据ID得到用户
	 * @param id
	 * @return
	 */
	public Classification findClassificationById(int id) {
		return Classification.dao.findById(id);
	}
}
