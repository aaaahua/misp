package edu.mis.modules.role;

import java.util.List;

import edu.mis.model.Classification;

/**
 * 权限服务类
 *
 */
public class RoleService {
	
	/**
	 * 查询所有权限
	 * @return
	 */
	public List<Classification> findAllRole(){
		return Classification.dao.findAll();
	}
}
