package edu.mis.modules.particles;

import java.util.List;

import com.jfinal.plugin.activerecord.Record;

import edu.mis.model.Particles;
import edu.mis.model.Particles;

/**
 * 用户服务类
 *
 */
public class ParticlesService {
	
	/**
	 * 注册
	 * @param particles
	 * @return
	 */
	public boolean register(Particles particles){
		if(particles.findParticlesByName(particles.getName())==null){ //此处为业务逻辑，写在service里，不要写在model里。判断是否用户名重复，
			return particles.save();
		}else{
			return false;
		}
		
	}
	
	/**
	 * 修改密码
	 * @param particles
	 * @return
	 */
	public boolean updatePrice(int id,int newPrice){
		Particles particles = findParticlesById(id);
		particles.setPrice(newPrice);
		return particles.update();
	}
	
	
	/**
	 * 登录校验
	 * @param name
	 * @param price
	 * 判断密码是否正确属于业务逻辑，应写在service中，尽管写在dao中和controller都能实现。
	 * dao中写基本的增删改查即可。dao中findParticlesByName可复用，不针对任何功能；
	 * controller仅仅建立模型（此处指service的loginValidate）和视图（此处指index.html,login.html）关系
	 */
	/*public boolean loginValidate(String name, String price){
		if(price.equals(Particles.dao.findParticlesByName(name).getPrice()))  
			return true; 
		else
			return false;
	}*/
	
	/**
	 * 根据ID得到用户
	 * @param id
	 * @return
	 */
	public Particles findParticlesById(int id) {
		Particles a=Particles.dao.findById(id);
		return Particles.dao.findById(id);
	}
	
	/**
	 * 查询所有用户
	 * @return
	 */
	public List<Particles> findAllParticles(){
		return Particles.dao.findAll();
	}
	/**
	 * 查询所有商品及其权限
	 * @return
	 */
	public List<Record> findAllParticlesWithClassification(){
		return Particles.dao.findAllRecord();
	}

	/**
	 * 用户授权
	 * @param id
	 * @param classificationId
	 * @return
	 */
	public boolean particlesAuthorize(int id, int classificationId) {
		Particles particles = findParticlesById(id);
		particles.setClassificationId(classificationId);
		return particles.update();
	}
}
