package edu.mis.modules.particles;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;

import edu.mis.model.Classification;
import edu.mis.model.Particles;

/**
 * 用户控制器
 *
 */
public class ParticlesController extends Controller {
	
	static ParticlesService service = new ParticlesService();
	
	public void index() {
		render("particles.html");
	}
	
	public void list(){
		setAttr("data", service.findAllParticlesWithClassification());
		renderJson();  
	}

	/**
	 * 注册用户视图
	 */
	public void add() {
		    //render("add.html");  //（默认对应add.html，否则需显性指定，如render("addParticles.html")）
	}

	public void register(){
		Particles particles  =  getModel(Particles.class);   //使用getModel方法获取表单值，表单中name应使用对象名.属性名，如particles.id, particles.price,详见jfinal手册
		if(service.register(particles)){
			setAttr("result", true); 
			setAttr("msg", "新建粒子成功!"); 
		}else{
			setAttr("result",false);
			setAttr("msg", "新建失败，用户名重复!"); 
		}
		renderJson(); 
	}
	
	/**
	 * 修改代号视图（默认对应edit.html，否则需显性指定，如render("editPrice.html")，）
	 */
	public void edit() {
		setAttr("particles", service.findParticlesById(getParaToInt("id")));  //此处particles应与表单中${(particles.name)!}等中的particles保持一致。
	}
	
	public void updatePrice(){
		int id=this.getParaToInt("id");   //使用getPara获取表单值，与getModel方法获得表单值不同，name不能使用对象名.属性名，如particles.price，而直接使用属性名，如price
		int newPrice = this.getParaToInt("price");
		if(service.updatePrice(id, newPrice)){
			setAttr("result", true);
			setAttr("msg", "修改代号成功!");
		}else{
			setAttr("result", false);
		}
		renderJson();
	}
	
	/**
	 * 用户授权视图
	 */
	public void auth() {
		setAttr("particles", service.findParticlesById(getParaToInt("id")));  //此处particles应与表单中${(particles.name)!}等中的particles保持一致。
		setAttr("classification",Classification.dao.findAll());//授权前得到所有权限
	}
	
	/**
	 * 用户授权
	 */
	public void authorize(){
		int id=this.getParaToInt("id");   
		int classificationId = this.getParaToInt("classificationId");
		if(service.particlesAuthorize(id, classificationId)){
			setAttr("result", true);
			setAttr("msg", "粒子分类成功!");
		}else{
			setAttr("result", false);
		}
		renderJson();
	}
	
	/*@Before(LoginValidator.class)   //执行前进行后端校验，用户名密码是否为空
	@Clear({LoginInterceptor.class}) //因为执行前用户还未登录，应清除登录拦截器。登录拦截器为全局拦截器，所有控制器默认都会被拦截
	public void loginValidate() {
		String name = getPara("name");
		String price = getPara("price") ;
		if(service.loginValidate(name,price)){
			setSessionAttr("loginParticles", name); //session记录登录用户名
			redirect("/index");
		}else{
			redirect("/login");
			setAttr("msg", "登录失败!");
		}		
	}*/

}


